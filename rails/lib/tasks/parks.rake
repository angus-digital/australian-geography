namespace :parks do
  desc "Fetch park data"
  task :fetch_geo, [:name] => [:environment] do |task, args|
    puts "Fetching park data for '#{args[:name]}'"
    data = get_geojson args[:name]
    park = Park.create **park_data(args[:name], data)
    p park.errors.full_messages.join("\n") unless park.valid?
    p park if park.valid?
  end

  def park_data(name, data)
    state = State.where(full_name: data["address"]["state"]).first
    if !state
      state = State.find_by_sql("select * from states where st_contains(geo_boundary_multi::geometry, st_transform(st_geomfromtext('POINT(#{data["lon"]} #{data["lat"]})', 4326), 4326)::geometry);").first
    end
    geojson = RGeo::GeoJSON.decode data["geojson"]
    return {
             :name => name,
             :slug => name.downcase.strip.gsub(" ", "-").gsub(/[^\w-]/, ""),
             :lat => data["lat"],
             :lng => data["lon"],
             :state => state,
             :geo_center => RGeo::Geographic.spherical_factory(srid: 4326).point(data["lon"], data["lat"]),
             :geo_boundary => (geojson if geojson.geometry_type.type_name == "Polygon"),
             :geo_boundary_multi => (geojson if geojson.geometry_type.type_name == "MultiPolygon"),
           }
  end

  def get_geojson(name)
    base_url = "https://nominatim.openstreetmap.org/search?format=json&polygon_geojson=1&addressdetails=1&countrycodes=au&q="
    doc = Nokogiri::HTML.parse(URI.open(base_url.concat(URI::encode(name))))
    data = JSON.parse(doc.text)
    return data[0] if data.count
  end
end
