class State < ApplicationRecord
  attribute :lat, :float
  attribute :lng, :float
  attribute :sw_lat, :float
  attribute :sw_lng, :float
  attribute :ne_lat, :float
  attribute :ne_lng, :float

  has_many :parks

  def geojson
    RGeo::GeoJSON.encode(self.geo_boundary || self.geo_boundary_multi, json_parser: :json).to_json
  end

  def area
    geometry = (self.geo_boundary || self.geo_boundary_multi)
    ActiveRecord::Base.connection.execute("
    SELECT
      ST_Area(
        ST_Transform(ST_GeomFromText('#{geometry.as_json}', #{geometry.srid}), 4326)::geography
      ) / 1000000 AS area;
    ")[0]["area"].to_f
  end

  def to_param
    slug
  end
end
