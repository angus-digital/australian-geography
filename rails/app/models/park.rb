class Park < ApplicationRecord
  belongs_to :state

  def geojson
    RGeo::GeoJSON.encode(self.geo_boundary || self.geo_boundary_multi, json_parser: :json)
  end

  def area
    geometry = (self.geo_boundary || self.geo_boundary_multi)
    if geometry
      ActiveRecord::Base.connection.execute("
      SELECT
        ST_Area(
          ST_Transform(ST_GeomFromText('#{geometry.as_json}', #{geometry.srid}), 4326)::geography
        ) / 1000000 AS area;
      ")[0]["area"].to_f
    end
  end

  def to_param
    slug
  end
end
