class Lake < ApplicationRecord
  def geojson
    RGeo::GeoJSON.encode(self.geo_boundary, json_parser: :json)
  end

  def area
    if self.geo_boundary
      ActiveRecord::Base.connection.execute("
      SELECT
        ST_Area(
          ST_Transform(ST_GeomFromText('#{self.geo_boundary.as_json}', #{self.geo_boundary.srid}), 4326)::geography
        ) / 1000000 AS area;
      ")[0]["area"].to_f
    end
  end

  def to_param
    slug
  end
end
