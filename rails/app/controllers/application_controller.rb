class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  respond_to? :json

  def park_path(park)
    state_park_path(park.state, park)
  end

  helper_method :park_path
end
