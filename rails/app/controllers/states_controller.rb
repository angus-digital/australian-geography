class StatesController < ApplicationController
  def index
    @states = State.includes(:parks).all
    respond_to do |format|
      format.html { render :index }
      format.json { render json: @states }
    end
  end

  def show
    @hide_nav = true
    @state = State.find_by! slug: params[:slug]
    respond_to do |format|
      format.html { render :state }
      format.json { render json: @state.attributes.except("geo_boundary", "geo_boundary_multi").merge(geojson: @state.geojson) }
    end
  end
end
