class LakesController < ApplicationController
  def index
    @lakes = Lake.all
    respond_to do |format|
      format.html { render :index }
      format.json { render json: @lakes }
    end
  end

  def show
    @hide_nav = true
    @lake = Lake.find_by! slug: params[:slug]
    respond_to do |format|
      format.html { render :lake }
      format.json { render json: @lake }
    end
  end
end
