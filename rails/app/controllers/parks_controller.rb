class ParksController < ApplicationController
  def index
    respond_to do |format|
      format.html {
        @states = State.includes(:parks).all
        render :index
      }
      format.json {
        @parks = Park.includes(:state).order(:state_id).all
        render json: @parks
      }
    end
  end

  def show
    @hide_nav = true
    @park = (params[:state_slug] ? (Park.includes(:state).where("states.slug" => params[:state_slug])) : Park).find_by! slug: params[:slug]
    respond_to do |format|
      format.html { render :park }
      format.json { render json: @park }
    end
  end

  def within_state
    @state = State.includes(:parks).find_by! slug: params[:state_slug]
    render :json => @state.parks.map { |park| park.attributes.except("geo_boundary", "geo_boundary_multi").merge(geojson: park.geojson) }
  end
end
