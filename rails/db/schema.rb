# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_19_095407) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "lakes", force: :cascade do |t|
    t.string "name"
    t.string "damn_name"
    t.string "poly_src"
    t.string "area_skm"
    t.float "long_deg"
    t.float "lat_deg"
    t.string "elev_m"
    t.string "volume_ckm"
    t.string "vol_src"
    t.string "country"
    t.string "sec_cntry"
    t.string "river"
    t.string "near_city"
    t.geography "geo_center", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.geography "geo_boundary", limit: {:srid=>4326, :type=>"st_polygon", :geographic=>true}
    t.string "slug"
    t.index ["geo_center"], name: "index_lakes_on_geo_center", using: :gist
    t.index ["lat_deg"], name: "index_lakes_on_lat_deg"
    t.index ["long_deg"], name: "index_lakes_on_long_deg"
    t.index ["name"], name: "index_lakes_on_name"
  end

  create_table "parks", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "state_id"
    t.string "name", limit: 255
    t.float "lat"
    t.float "lng"
    t.text "overview"
    t.string "official_url", limit: 255
    t.string "slug", limit: 100
    t.string "timezone", default: "Australia/Sydney"
    t.geography "geo_center", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.geography "geo_boundary", limit: {:srid=>4326, :type=>"st_polygon", :geographic=>true}
    t.geography "geo_boundary_multi", limit: {:srid=>4326, :type=>"multi_polygon", :geographic=>true}
    t.index ["geo_boundary"], name: "index_parks_on_geo_boundary", using: :gist
    t.index ["geo_boundary_multi"], name: "index_parks_on_geo_boundary_multi", using: :gist
    t.index ["geo_center"], name: "index_parks_on_geo_center", using: :gist
    t.index ["lat"], name: "index_parks_on_lat"
    t.index ["lng"], name: "index_parks_on_lng"
    t.index ["name"], name: "index_parks_on_name"
    t.index ["slug", "state_id"], name: "index_parks_on_slug_and_state_id", unique: true
    t.index ["state_id"], name: "index_parks_on_state_id"
  end

  create_table "states", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "full_name", limit: 255, null: false
    t.string "abbrv_name", limit: 255, null: false
    t.string "full_slug", limit: 100
    t.string "slug", limit: 100
    t.decimal "lat"
    t.decimal "lng"
    t.decimal "sw_lat"
    t.decimal "sw_lng"
    t.decimal "ne_lat"
    t.decimal "ne_lng"
    t.geography "geo_center", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.geography "geo_boundary", limit: {:srid=>4326, :type=>"st_polygon", :geographic=>true}
    t.geography "geo_boundary_multi", limit: {:srid=>4326, :type=>"multi_polygon", :geographic=>true}
    t.string "seo_title", limit: 100
    t.string "seo_description", limit: 255
    t.string "display_title", limit: 255
    t.index ["full_slug"], name: "index_states_on_full_slug", unique: true
    t.index ["slug"], name: "index_states_on_slug", unique: true
  end

end
