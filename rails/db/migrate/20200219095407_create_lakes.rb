class CreateLakes < ActiveRecord::Migration[6.0]
  def change
    create_table :lakes do |t|
      t.string "name"
      t.string "damn_name"
      t.string "poly_src"
      t.string "area_skm"
      t.float "long_deg"
      t.float "lat_deg"
      t.string "elev_m"
      t.string "volume_ckm"
      t.string "vol_src"
      t.string "country"
      t.string "sec_cntry"
      t.string "river"
      t.string "near_city"
      t.geography "geo_center", limit: { :srid => 4326, :type => "st_point", :geographic => true }
      t.geography "geo_boundary", limit: { :srid => 4326, :type => "st_polygon", :geographic => true }
      t.string "slug"

      t.index ["geo_center"], name: "index_lakes_on_geo_center", using: :gist
      t.index ["lat_deg"], name: "index_lakes_on_lat_deg"
      t.index ["long_deg"], name: "index_lakes_on_long_deg"
      t.index ["name"], name: "index_lakes_on_name"
    end
  end
end
