class CreateStates < ActiveRecord::Migration[6.0]
  def change
    create_table :states do |t|
      t.timestamps
      t.string "full_name", limit: 255, null: false
      t.string "abbrv_name", limit: 255, null: false
      t.string "full_slug", limit: 100
      t.string "slug", limit: 100
      t.decimal "lat"
      t.decimal "lng"
      t.decimal "sw_lat"
      t.decimal "sw_lng"
      t.decimal "ne_lat"
      t.decimal "ne_lng"
      t.geography "geo_center", limit: { srid: 4326, type: "st_point", geographic: true }
      t.geography "geo_boundary", limit: { srid: 4326, type: "st_polygon", geographic: true }
      t.geography "geo_boundary_multi", limit: { srid: 4326, type: "multi_polygon", geographic: true }
      t.string "seo_title", limit: 100
      t.string "seo_description", limit: 255
      t.string "display_title", limit: 255

      t.index ["full_slug"], name: "index_states_on_full_slug", unique: true
      t.index ["slug"], name: "index_states_on_slug", unique: true
    end
  end
end
