class CreateParks < ActiveRecord::Migration[6.0]
  def change
    create_table :parks do |t|
      t.timestamps
      t.belongs_to :state
      t.string "name", limit: 255
      t.float "lat"
      t.float "lng"
      t.text "overview"
      t.string "official_url", limit: 255
      t.string "slug", limit: 100
      t.string "timezone", default: "Australia/Sydney"
      t.geography "geo_center", limit: { srid: 4326, type: "st_point", geographic: true }
      t.geography "geo_boundary", limit: { srid: 4326, type: "st_polygon", geographic: true }
      t.geography "geo_boundary_multi", limit: { srid: 4326, type: "multi_polygon", geographic: true }

      t.index ["geo_center"], name: "index_parks_on_geo_center", using: :gist
      t.index ["lat"], name: "index_parks_on_lat"
      t.index ["lng"], name: "index_parks_on_lng"
      t.index ["name"], name: "index_parks_on_name"
      t.index %w[slug state_id], name: "index_parks_on_slug_and_state_id", unique: true
      t.index ["state_id"], name: "index_parks_on_state_id"
    end
  end
end
