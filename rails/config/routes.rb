Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "home#index"
  get "home", to: "home#main"
  resources :lakes, only: [:index, :show], param: :slug
  resources :parks, only: [:index, :show], param: :slug
  resources :states, only: [:index, :show], param: :slug do
    get "parks", to: "parks#within_state"
    resources :parks, only: [:index, :show], param: :slug
  end
end
